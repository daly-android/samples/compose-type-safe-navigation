package com.daly.compose_typesafe_navigation

import kotlinx.serialization.Serializable

object Screen {
    @Serializable
    object One

    @Serializable
    data class Two(val param: String)
}