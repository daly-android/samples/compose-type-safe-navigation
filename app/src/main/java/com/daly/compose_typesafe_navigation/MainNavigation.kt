package com.daly.compose_typesafe_navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.toRoute

@Composable
fun MainNavigation(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Screen.One
    ) {
        composable<Screen.One> {
            ScreenOne(
                onClickAction = {
                    navController.navigate(
                        Screen.Two(param = "Param 1")
                    )
                }
            )
        }

        composable<Screen.Two> {
            val args = it.toRoute<Screen.Two>()
            ScreenTwo(
                param = args.param,
            )
        }
    }
}